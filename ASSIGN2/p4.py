from p1 import encrypt, decrypt
from p2 import generate_custom_entropy



# Example usage

plaintext = b"Text to encrypt"

key,_,_,_ = generate_custom_entropy(len(plaintext))

ciphertext = encrypt(plaintext, key)
decrypted_message = decrypt(ciphertext, key)

print("Original message:", plaintext)
print("Key:", key)
print("Ciphertext:", ciphertext)
print("Decrypted message:", decrypted_message)