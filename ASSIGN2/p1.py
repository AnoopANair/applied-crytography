import os

def generate_key(length):
    # Use a strong entropy source to generate a random key
    return os.urandom(length)

def encrypt(message, key):
    # Ensure the message and key are of the same length
    print(len(message))
    print(len(key))

    if len(message) != len(key):
        raise ValueError("Message and key lengths must be the same")


    # XOR each byte of the message with the corresponding byte of the key
    ciphertext = bytes([m ^ k for m, k in zip(bytes(message), bytes(key,encoding='utf8'))])
    return ciphertext

def decrypt(ciphertext, key):
    # XOR each byte of the ciphertext with the corresponding byte of the key
    message = bytes([c ^ k for c, k in zip(bytes(ciphertext), bytes(key,encoding='utf8'))])
    return message

# # Example usage
# message = b"Hello, World!"
# key = generate_key(len(message))

# ciphertext = encrypt(message, key)
# decrypted_message = decrypt(ciphertext, key)

# print("Original message:", message)
# print("Key:", key)
# print("Ciphertext:", ciphertext)
# print("Decrypted message:", decrypted_message)


# OUTPUT

# Original message: b'Hello, World!'
# Key: b'\x7fN\xc8\t\x11\xae\r2\xc4\x07\x17\xf8:'
# Ciphertext: b'7+\xa4e~\x82-e\xabu{\x9c\x1b'
# Decrypted message: b'Hello, World!'

