




import time
import random
import pyautogui

def gather_time_entropy():
    # Use the current time in milliseconds as entropy
    current_time = int(time.time() * 1000)
    return current_time.to_bytes(8, 'big')

def gather_mouse_entropy():
    # Get mouse position
    x, y = pyautogui.position()

    # Use x and y coordinates as entropy
    entropy = x.to_bytes(4, 'big') + y.to_bytes(4, 'big')
    return entropy

def gather_keyboard_entropy():
    # Simulate typing to capture keyboard timings
    random_string = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(10))
    start_time = time.time()

    # Type a random string
    pyautogui.typewrite(random_string)

    # Measure the time taken to type
    end_time = time.time()
    typing_time = int((end_time - start_time) * 1000)

    # Use typing time as entropy
    return typing_time.to_bytes(4, 'big')

def generate_custom_entropy(length):
    # Gather entropy from different sources
    time_entropy = gather_time_entropy()
    mouse_entropy = gather_mouse_entropy()
    keyboard_entropy = gather_keyboard_entropy()

    # Combine entropy from all sources
    combined_entropy = time_entropy + mouse_entropy + keyboard_entropy

    # Hash the combined entropy to generate a final key
    # print(combined_entropy)

    print(hash(time_entropy))
    print(hash(mouse_entropy))
    print(hash(keyboard_entropy))
    print(hash(combined_entropy))

    time_key = str(hash(time_entropy))[:length]
    mouse_key = str(hash(mouse_entropy))[:length]
    keyboard_key = str(hash(keyboard_entropy))[:length]
    final_key = str(hash(combined_entropy))[:length]
    # print(final_key)


    # final_key = hash(combined_entropy)[:length]
    return final_key, time_key, mouse_key, keyboard_key

# # Example usage
# custom_entropy_key = generate_custom_entropy(16)
# print("Custom Entropy Key:", custom_entropy_key)

# OUTPUT
# Custom Entropy Key: -8143533621265485