

import os
import math
import random
from p2 import generate_custom_entropy

def shannon_entropy(data):
    probabilities = [float(data.count(c)) / len(data) for c in set(data)]
    entropy = -sum(p * math.log2(p) for p in probabilities)
    return entropy

# from random module 

rand_seq = [random.randint(0,100) for _ in range(256)]
random_bytes = bytes(rand_seq)
random_entropy = shannon_entropy(random_bytes)

print("Entropy using random module:", random_entropy)


# Using os.urandom() for comparison
os_urandom_key = os.urandom(256)
os_urandom_entropy = shannon_entropy(os_urandom_key)

print("Entropy using os.urandom():", os_urandom_entropy)


final_key, time_key, mouse_key, keyboard_key = generate_custom_entropy(256)
custom_entropy_entropy = shannon_entropy(final_key)
time_entropy = shannon_entropy(time_key)
mouse_entropy = shannon_entropy(mouse_key)
keyboard_entropy = shannon_entropy(keyboard_key)

print("Entropy using custom entropy generator:", custom_entropy_entropy)
print("Entropy using time entropy generator:", time_entropy)
print("Entropy using mouse entropy generator:", mouse_entropy)
print("Entropy using keyboard entropy generator:", keyboard_entropy)

# OUTPUT
# Entropy using random module: 6.351344461577976
# Entropy using os.urandom(): 7.186160693198806
# Entropy using custom entropy generator: 2.9841837197791885

# Entropy using custom entropy generator: 3.010570934268483
# Entropy using time entropy generator: 3.0441104177484006
# Entropy using mouse entropy generator: 3.0761031709967224
# Entropy using keyboard entropy generator: 2.7550576199383077