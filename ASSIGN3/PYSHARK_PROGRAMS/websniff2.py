import pyshark

# Define a display filter to capture HTTP traffic
display_filter = 'http'

# Capture HTTP packets on the default interface
capture = pyshark.LiveCapture(display_filter=display_filter)

# Sniff packets and analyze HTTP traffic
for packet in capture.sniff_continuously():
    try:
        # Check if packet has HTTP layer
        if packet.http:
            # Extract relevant information
            method = packet.http.request_method
            uri = packet.http.request_uri
            headers = packet.http._all_fields.get('headers', '')
            data = packet.http.get_field_by_showname('Hypertext Transfer Protocol').show
            # Print out the information
            print(f"HTTP Method: {method}")
            print(f"URI: {uri}")
            print(f"Headers: {headers}")
            print(f"Data: {data}")
            print("=" * 50)
    except AttributeError:
        pass  # Skip packets that don't have HTTP layer
