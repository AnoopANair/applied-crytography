const express = require('express');
const http = require('http');
const path = require("path");
const bodyParser = require('body-parser');
const crypto = require('crypto');
const users = require('./data').userDB;

const app = express();
const server = http.createServer(app);

app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname,'./public')));

// RSA Key Pair
const { publicKey, privateKey } = crypto.generateKeyPairSync('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
    },
    privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
    }
});

// Function to encrypt using RSA public key
function encryptWithPublicKey(text) {
    const buffer = Buffer.from(text, 'utf8');
    return crypto.publicEncrypt(publicKey, buffer).toString('base64');
}

// Function to decrypt using RSA private key
function decryptWithPrivateKey(text) {
    const buffer = Buffer.from(text, 'base64');
    return crypto.privateDecrypt(privateKey, buffer).toString('utf8');
}

app.get('/',(req,res) => {
    res.sendFile(path.join(__dirname,'./public/index.html'));
});

app.post('/register', async (req, res) => {
    try {
        let foundUser = users.find((data) => req.body.email === data.email);
        if (!foundUser) {
            let encryptedPassword = encryptWithPublicKey(req.body.password); // Encrypting the password
            let newUser = {
                id: Date.now(),
                username: req.body.username,
                email: req.body.email,
                password: encryptedPassword,
            };
            users.push(newUser);
            console.log('User list', users);
            res.send("<div align ='center'><h2>Registration successful</h2></div><br><br><div align='center'><a href='./login.html'>login</a></div><br><br><div align='center'><a href='./registration.html'>Register another user</a></div>");
        } else {
            res.send("<div align ='center'><h2>Email already used</h2></div><br><br><div align='center'><a href='./registration.html'>Register again</a></div>");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error");
    }
});

app.post('/login', async (req, res) => {
    try {
        let foundUser = users.find((data) => req.body.email === data.email);
        if (foundUser) {
            let decryptedPassword = decryptWithPrivateKey(foundUser.password); // Decrypting the password
            if (req.body.password === decryptedPassword) { // Comparing with the provided password
                let usrname = foundUser.username;
                res.send(`<div align ='center'><h2>Login successful</h2></div><br><br><br><div align ='center'><h3>Hello ${usrname}</h3></div><br><br><div align='center'><a href='./login.html'>Logout</a></div>`);
            } else {
                res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align ='center'><a href='./login.html'>Login again</a></div>");
            }
        } else {
            res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align='center'><a href='./login.html'>Login again</a><div>");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error");
    }
});

app.get('/adminGetUsers', (req,res) => {
    // Don't expose sensitive data like passwords in admin endpoints in a real application
    res.send(users);
});

server.listen(3000, function(){
    console.log("server is listening on port: 3000");
});
