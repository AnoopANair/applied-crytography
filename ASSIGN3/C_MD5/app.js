const express = require('express');
const https = require('https');
const fs = require('fs');
const path = require("path");
const bodyParser = require('body-parser');
const crypto = require('crypto'); // Added for MD5 hashing
const users = require('./data').userDB;

const app = express();

// Body parser and static file serving middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname,'./public')));

// Routes
app.get('/',(req,res) => {
    res.sendFile(path.join(__dirname,'./public/index.html'));
});

// Registration endpoint
app.post('/register', async (req, res) => {
    try {
        let foundUser = users.find((data) => req.body.email === data.email);
        if (!foundUser) {
            // Hash the password using MD5
            const hashedPassword = crypto.createHash('md5').update(req.body.password).digest('hex');

            let newUser = {
                id: Date.now(),
                username: req.body.username,
                email: req.body.email,
                password: hashedPassword, // Store hashed password in the database
            };
            users.push(newUser);
            console.log('User list', users);
            res.send("<div align ='center'><h2>Registration successful</h2></div><br><br><div align='center'><a href='./login.html'>login</a></div><br><br><div align='center'><a href='./registration.html'>Register another user</a></div>");
        } else {
            res.send("<div align ='center'><h2>Email already used</h2></div><br><br><div align='center'><a href='./registration.html'>Register again</a></div>");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error");
    }
});

// Login endpoint
app.post('/login', async (req, res) => {
    try {
        let foundUser = users.find((data) => req.body.email === data.email);
        if (foundUser) {
            let submittedPass = req.body.password; 
            let storedPass = foundUser.password; 
            // Hash the submitted password and compare with stored hashed password
            const submittedHashedPassword = crypto.createHash('md5').update(submittedPass).digest('hex');
            const passwordMatch = submittedHashedPassword === storedPass;

            if (passwordMatch) {
                let usrname = foundUser.username;
                res.send(`<div align ='center'><h2>Login successful</h2></div><br><br><br><div align ='center'><h3>Hello ${usrname}</h3></div><br><br><div align='center'><a href='./login.html'>Logout</a></div>`);
            } else {
                res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align ='center'><a href='./login.html'>Login again</a></div>");
            }
        } else {
            res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align='center'><a href='./login.html'>Login again</a><div>");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error");
    }
});

// Admin endpoint
app.get('/adminGetUsers', (req,res) => {
    // In a real-world scenario, you should have authentication and authorization before providing access to admin functionalities.
    // For simplicity, we're just sending the user list here.
    res.send(users);
});

// HTTPS options
const options = {
    key: fs.readFileSync('./private-key.pem'),
    cert: fs.readFileSync('./certificate.pem')
};

// Create HTTPS server
const server = https.createServer(options, app);

// Listen to the server
server.listen(3000, function(){
    console.log("Server is listening on port: 3000");
});
