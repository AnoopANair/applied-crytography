const express = require('express');
const https = require('https');
const fs = require('fs')
const path = require("path");
const bodyParser = require('body-parser');
const users = require('./data').userDB;
const CryptoJS = require('crypto-js'); // Import CryptoJS library

const app = express();


app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname,'./public')));

// Caesar Cipher Encryption and Decryption Functions
function caesarCipherEncrypt(text, shift) {
    return text.split('').map(char => {
        let code = char.charCodeAt(0);
        if ((code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
            let offset = code >= 65 && code <= 90 ? 65 : 97;
            return String.fromCharCode(((code - offset + shift) % 26) + offset);
        }
        return char;
    }).join('');
}

function caesarCipherDecrypt(text, shift) {
    return caesarCipherEncrypt(text, (26 - shift) % 26);
}

app.get('/',(req,res) => {
    res.sendFile(path.join(__dirname,'./public/index.html'));
});

app.post('/register', async (req, res) => {
    try {
        let foundUser = users.find((data) => req.body.email === data.email);
        if (!foundUser) {
            // Encrypt the password before storing it
            let encryptedPassword = caesarCipherEncrypt(req.body.password, 3);
            let newUser = {
                id: Date.now(),
                username: req.body.username,
                email: req.body.email,
                password: encryptedPassword,
            };
            users.push(newUser);
            console.log('User list', users);
            res.send("<div align ='center'><h2>Registration successful</h2></div><br><br><div align='center'><a href='./login.html'>login</a></div><br><br><div align='center'><a href='./registration.html'>Register another user</a></div>");
        } else {
            res.send("<div align ='center'><h2>Email already used</h2></div><br><br><div align='center'><a href='./registration.html'>Register again</a></div>");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error");
    }
});

app.post('/login', async (req, res) => {
    try {
        let foundUser = users.find((data) => req.body.email === data.email);
        if (foundUser) {
            // Decrypt the password for comparison
            let decryptedPassword = caesarCipherDecrypt(foundUser.password, 3);
            if (req.body.password === decryptedPassword) {
                let usrname = foundUser.username;
                res.send(`<div align ='center'><h2>Login successful</h2></div><br><br><br><div align ='center'><h3>Hello ${usrname}</h3></div><br><br><div align='center'><a href='./login.html'>Logout</a></div>`);
            } else {
                res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align ='center'><a href='./login.html'>Login again</a></div>");
            }
        } else {
            res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align='center'><a href='./login.html'>Login again</a><div>");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal server error");
    }
});

app.get('/adminGetUsers', (req,res) => {
    // Don't expose sensitive data like passwords in admin endpoints in a real application
    res.send(users);
});


// HTTPS options
const options = {
    key: fs.readFileSync('./private-key.pem'),
    cert: fs.readFileSync('./certificate.pem')
};


const server = https.createServer(options, app);


server.listen(3000, function(){
    console.log("server is listening on port: 3000");
});
